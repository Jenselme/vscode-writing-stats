export const getNumberOfWords = (text: string): number => {
  const matches = text.match(/[\p{Letter}\p{Number}]+/giu);

  if (matches) {
    return matches.length;
  }

  return 0;
};

export type NumberOfCharactersOptions = {
  countFormattingCharacters: boolean;
  countNumberOfSpaces: boolean;
};

const formattingCharacters = ["*", "_", "`", "#", "=", "-", "~", ">", "\\", "[", "]", "<", ">"];

export const getNumberOfCharacters = (
  text: string,
  { countFormattingCharacters, countNumberOfSpaces }: NumberOfCharactersOptions,
): number => {
  return text.split("").filter((char) => {
    const excludeFormattingCharacter =
      !countFormattingCharacters && formattingCharacters.includes(char);
    const excludeSpace = !countNumberOfSpaces && [" ", "\n"].includes(char);

    return !excludeFormattingCharacter && !excludeSpace;
  }).length;
};

export type NumberOfLinesOptions = {
  countEmptyLines: boolean;
};

export const getNumberOfLines = (
  text: string,
  { countEmptyLines }: NumberOfLinesOptions,
): number => {
  return text.split("\n").filter((line) => (countEmptyLines ? true : line.trim().length > 0))
    .length;
};
