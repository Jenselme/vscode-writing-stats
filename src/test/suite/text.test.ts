import * as assert from "assert";
import { getNumberOfCharacters, getNumberOfLines, getNumberOfWords } from "../../text";

/* eslint-disable max-len */
const textSample = `---
__Advertisement :)__

- __[pica](https://nodeca.github.io/pica/demo/)__ - high quality and fast image
resize in browser.
- __[babelfish](https://github.com/nodeca/babelfish/)__ - developer friendly
i18n with plurals support and easy syntax.

You will like those projects!
---

# h1 Heading 8-)

## h2 Heading

### h3 Heading

#### h4 Heading

##### h5 Heading

###### h6 Heading

## Horizontal Rules

---

---

---

## Typographic replacements

Enable typographer option to see result.

(c) (C) (r) (R) (tm) (TM) (p) (P) +-

test.. test... test..... test?..... test!....

!!!!!! ???? ,, -- ---

"Smartypants, double quotes" and 'single quotes'

## Emphasis

**This is bold text**

**This is bold text**

_This is italic text_

_This is italic text_

~~Strikethrough~~

## Blockquotes

> Blockquotes can also be nested...
>
> > ...by using additional greater-than signs right next to each other...
> >
> > > ...or with spaces between arrows.

## Lists

Unordered

- Create a list by starting a line with \`+\`, \`-\`, or \`*\`
- Sub-lists are made by indenting 2 spaces:
  - Marker character change forces new list start:
    - Ac tristique libero volutpat at
    * Facilisis in pretium nisl aliquet
    - Nulla volutpat aliquam velit
- Very easy!

Ordered

1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa

4. You can use sequential numbers...
5. ...or keep all the numbers as \`1.\`

Start numbering with offset:

57. foo
1. bar

## Code

Inline \`code\`

Indented code

    // Some comments
    line 1 of code
    line 2 of code
    line 3 of code

Block code "fences"

\`\`\`
Sample text here...
\`\`\`

Syntax highlighting

\`\`\`js
var foo = function (bar) {
  return bar++;
};

console.log(foo(5));
\`\`\`

## Tables

| Option | Description                                                               |
| ------ | ------------------------------------------------------------------------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default.    |
| ext    | extension to be used for dest files.                                      |

Right aligned columns

| Option |                                                               Description |
| -----: | ------------------------------------------------------------------------: |
|   data | path to data files to supply the data that will be passed into templates. |
| engine |    engine to be used for processing templates. Handlebars is the default. |
|    ext |                                      extension to be used for dest files. |

## Links

[link text](http://dev.nodeca.com)

[link with title](http://nodeca.github.io/pica/demo/ "title text!")

Autoconverted link https://github.com/nodeca/pica (enable linkify to see)

## Images

![Minion](https://octodex.github.com/images/minion.png)
![Stormtroopocat](https://octodex.github.com/images/stormtroopocat.jpg "The Stormtroopocat")

Like links, Images also have a footnote style syntax

![Alt text][id]

With a reference later in the document defining the URL location:

[id]: https://octodex.github.com/images/dojocat.jpg "The Dojocat"

## Plugins

The killer feature of \`markdown-it\` is very effective support of
[syntax plugins](https://www.npmjs.org/browse/keyword/markdown-it-plugin).

### [Emojies](https://github.com/markdown-it/markdown-it-emoji)

> Classic markup: :wink: :crush: :cry: :tear: :laughing: :yum:
>
> Shortcuts (emoticons): :-) :-( 8-) ;)

see [how to change output](https://github.com/markdown-it/markdown-it-emoji#change-output) with twemoji.

### [Subscript](https://github.com/markdown-it/markdown-it-sub) / [Superscript](https://github.com/markdown-it/markdown-it-sup)

- 19^th^
- H~2~O

### [\<ins>](https://github.com/markdown-it/markdown-it-ins)

++Inserted text++

### [\<mark>](https://github.com/markdown-it/markdown-it-mark)

==Marked text==

### [Footnotes](https://github.com/markdown-it/markdown-it-footnote)

Footnote 1 link[^first].

Footnote 2 link[^second].

Inline footnote^[Text of inline footnote] definition.

Duplicated footnote reference[^second].

[^first]: Footnote **can have markup**

    and multiple paragraphs.

[^second]: Footnote text.

### [Definition lists](https://github.com/markdown-it/markdown-it-deflist)

Term 1

: Definition 1
with lazy continuation.

Term 2 with _inline markup_

: Definition 2

        { some code, part of Definition 2 }

    Third paragraph of definition 2.

_Compact style:_

Term 1
~ Definition 1

Term 2
~ Definition 2a
~ Definition 2b

### [Abbreviations](https://github.com/markdown-it/markdown-it-abbr)

This is HTML abbreviation example.

It converts "HTML", but keep intact partial entries like "xxxHTMLyyy" and so on.

\*[HTML]: Hyper Text Markup Language

### [Custom containers](https://github.com/markdown-it/markdown-it-container)

::: warning
_here be dragons_
:::
`;
/* eslint-enable */

suite("Test getNumberOfWords", () => {
  test("Basics", () => {
    assert.strictEqual(getNumberOfWords("Hello World"), 2);
    assert.strictEqual(getNumberOfWords("Hello.World"), 2);
    assert.strictEqual(getNumberOfWords("Hello. World"), 2);
    assert.strictEqual(getNumberOfWords("Hello"), 1);
    assert.strictEqual(getNumberOfWords("# Hello"), 1);
    assert.strictEqual(getNumberOfWords("Hello 11"), 2);
    assert.strictEqual(getNumberOfWords("Hello11"), 1);
    assert.strictEqual(getNumberOfWords("Hello *World!*. This _is_ VSCode."), 5);
  });

  test("Full file", () => {
    assert.strictEqual(getNumberOfWords(textSample), 633);
  });
});

suite("Test getNumberOfCharacters", () => {
  test("Basics", () => {
    const options = {
      countFormattingCharacters: true,
      countNumberOfSpaces: true,
    };

    assert.strictEqual(getNumberOfCharacters("Hello World", options), 11);
    assert.strictEqual(getNumberOfCharacters("Hello", options), 5);
    assert.strictEqual(getNumberOfCharacters("Hello 11", options), 8);
    assert.strictEqual(getNumberOfCharacters("Hello11", options), 7);
    assert.strictEqual(getNumberOfCharacters("Hello *World!*. This _is_ VSCode.", options), 33);
    assert.strictEqual(getNumberOfCharacters(`Hello *World!*.\nThis _is_ VSCode.`, options), 33);
  });

  test("Basics no spaces", () => {
    const options = {
      countFormattingCharacters: true,
      countNumberOfSpaces: false,
    };

    assert.strictEqual(getNumberOfCharacters("Hello World", options), 10);
    assert.strictEqual(getNumberOfCharacters("Hello", options), 5);
    assert.strictEqual(getNumberOfCharacters("Hello *World!*. This _is_ VSCode.", options), 29);
  });

  test("Basics ignore formatting characters", () => {
    const options = {
      countFormattingCharacters: false,
      countNumberOfSpaces: true,
    };

    assert.strictEqual(getNumberOfCharacters("Hello World", options), 11);
    assert.strictEqual(getNumberOfCharacters("Hello", options), 5);
    assert.strictEqual(getNumberOfCharacters("Hello *World!*. This _is_ VSCode.", options), 29);
  });

  test("Basics ignore formatting characters & spaces", () => {
    const options = {
      countFormattingCharacters: false,
      countNumberOfSpaces: false,
    };

    assert.strictEqual(getNumberOfCharacters("Hello World", options), 10);
    assert.strictEqual(getNumberOfCharacters("Hello", options), 5);
    assert.strictEqual(getNumberOfCharacters("Hello *World!*. This _is_ VSCode.", options), 25);
  });

  test("Full file", () => {
    assert.strictEqual(
      getNumberOfCharacters(textSample, {
        countFormattingCharacters: true,
        countNumberOfSpaces: true,
      }),
      4928,
    );

    assert.strictEqual(
      getNumberOfCharacters(textSample, {
        countFormattingCharacters: false,
        countNumberOfSpaces: true,
      }),
      4487,
    );

    assert.strictEqual(
      getNumberOfCharacters(textSample, {
        countFormattingCharacters: true,
        countNumberOfSpaces: false,
      }),
      3973,
    );

    assert.strictEqual(
      getNumberOfCharacters(textSample, {
        countFormattingCharacters: false,
        countNumberOfSpaces: false,
      }),
      3532,
    );
  });
});

suite("getNumberOfLines", () => {
  test("Basics", () => {
    const options = {
      countEmptyLines: true,
    };

    assert.strictEqual(getNumberOfLines("Hello World", options), 1);
    assert.strictEqual(getNumberOfLines("Hello\nWorld", options), 2);
    assert.strictEqual(getNumberOfLines("Hello\n\nWorld", options), 3);
  });

  test("Ignore empty lines", () => {
    const options = {
      countEmptyLines: false,
    };

    assert.strictEqual(getNumberOfLines("Hello World", options), 1);
    assert.strictEqual(getNumberOfLines("Hello\nWorld", options), 2);
    assert.strictEqual(getNumberOfLines("Hello\n\nWorld", options), 2);
  });

  test("Full file", () => {
    assert.strictEqual(
      getNumberOfLines(textSample, {
        countEmptyLines: true,
      }),
      233,
    );

    assert.strictEqual(
      getNumberOfLines(textSample, {
        countEmptyLines: false,
      }),
      140,
    );
  });
});
