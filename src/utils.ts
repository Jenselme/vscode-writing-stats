import { extname } from "path";
import { getAllowedExtension } from "./configuration";

export const isFileExtensionAllowed = (fileName: string): boolean => {
  const allowedExtension: string[] = getAllowedExtension();
  return allowedExtension.includes(extname(fileName));
};

export const isFileExtensionMarkdown = (fileName: string): boolean => {
  return extname(fileName) === ".md";
};

const removeMarkupFunctionCreator = Promise.all([
  import("unified"),
  import("remark-gfm"),
  import("remark-parse"),
  import("remark-remove"),
]);

export const removeMarkdownSyntax = async (text: string): Promise<string> => {
  const [{ unified }, { default: remarkGfm }, { default: remarkParse }, { default: remarkRemove }] =
    await removeMarkupFunctionCreator;
  const result = await unified().use(remarkParse).use(remarkGfm).use(remarkRemove).process(text);

  return result.value.toString();
};
