import * as vscode from "vscode";

const configurationPrefix = "writingStats";

export type AvailableStats = "words" | "characters" | "lines" | "readingTime";

export const getAllowedExtension = (): string[] => {
  return vscode.workspace
    .getConfiguration(configurationPrefix)
    .allowedExtension.split(",")
    .map((ext: string) => ext.trim());
};

export const getCountFormattingCharacters = (): boolean => {
  return vscode.workspace.getConfiguration(configurationPrefix).countFormattingCharacters;
};

export const getCountSpaces = (): boolean => {
  return vscode.workspace.getConfiguration(configurationPrefix).countSpaces;
};

export const getCountEmptyLines = (): boolean => {
  return vscode.workspace.getConfiguration(configurationPrefix).countEmptyLines;
};

export const getDisplaySelectionStats = (): boolean => {
  return vscode.workspace.getConfiguration(configurationPrefix).displaySelectionStats;
};

export const getRenderedTextStats = (): boolean => {
  return vscode.workspace.getConfiguration(configurationPrefix).displayRenderedStats;
};

export const getStatsToDisplay = (): AvailableStats[] => {
  const availableStats: AvailableStats[] = [];
  if (vscode.workspace.getConfiguration(configurationPrefix).displayWordsCount) {
    availableStats.push("words");
  }
  if (vscode.workspace.getConfiguration(configurationPrefix).displayCharactersCount) {
    availableStats.push("characters");
  }
  if (vscode.workspace.getConfiguration(configurationPrefix).displayLinesCount) {
    availableStats.push("lines");
  }
  if (vscode.workspace.getConfiguration(configurationPrefix).displayReadingTime) {
    availableStats.push("readingTime");
  }

  return availableStats;
};

export const getWordsPerMinutes = (): number => {
  return vscode.workspace.getConfiguration(configurationPrefix).wordsPerMinutes;
};
