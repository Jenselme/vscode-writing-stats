import * as vscode from "vscode";
import {
  NumberOfCharactersOptions,
  NumberOfLinesOptions,
  getNumberOfCharacters,
  getNumberOfLines,
  getNumberOfWords,
} from "./text";
import { isFileExtensionAllowed, isFileExtensionMarkdown, removeMarkdownSyntax } from "./utils";
import {
  AvailableStats,
  getCountEmptyLines,
  getCountFormattingCharacters,
  getCountSpaces,
  getDisplaySelectionStats,
  getRenderedTextStats,
  getStatsToDisplay,
  getWordsPerMinutes,
} from "./configuration";

type TextStats = Record<AvailableStats, number>;

const emptyStats: TextStats = {
  words: 0,
  characters: 0,
  lines: 0,
  readingTime: 0,
} as const;

export class Stats {
  private statusBar: vscode.StatusBarItem;
  private editor: vscode.TextEditor | null;

  constructor() {
    this.editor = null;
    this.statusBar = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left);
  }

  private async update() {
    const fileStats = this.getFileStats();
    const selectedTextStats = this.getSelectedTextStats();
    const renderedTextStats = await this.getRenderedTextStats();
    const stats: string[] = [];
    const statsToDisplay = getStatsToDisplay();

    for (const label of Object.keys(fileStats)) {
      if (!statsToDisplay.includes(label as AvailableStats)) {
        continue;
      }

      stats.push(
        this.formatStat(
          label,
          fileStats[label as AvailableStats],
          selectedTextStats[label as AvailableStats],
          renderedTextStats[label as AvailableStats],
        ),
      );
    }

    this.statusBar.text = stats.join(" | ");
  }

  private getFileStats(): TextStats {
    if (!this.editor) {
      return emptyStats;
    }

    return this.getTextStats(this.editor.document.getText());
  }

  private getTextStats(text: string): TextStats {
    const nbOfWords = getNumberOfWords(text);
    return {
      words: nbOfWords,
      characters: getNumberOfCharacters(text, this.getNumberOfCharactersOptions()),
      lines: getNumberOfLines(text, this.getNumberOfLinesOptions()),
      readingTime: nbOfWords / getWordsPerMinutes(),
    };
  }

  private getNumberOfCharactersOptions(): NumberOfCharactersOptions {
    return {
      countFormattingCharacters: getCountFormattingCharacters(),
      countNumberOfSpaces: getCountSpaces(),
    };
  }

  private getNumberOfLinesOptions(): NumberOfLinesOptions {
    return {
      countEmptyLines: getCountEmptyLines(),
    };
  }

  private getSelectedTextStats(): TextStats {
    if (!this.editor) {
      return emptyStats;
    }

    return this.editor.selections
      .map((selection): TextStats => {
        const selectedRange = new vscode.Range(
          selection.start.line,
          selection.start.character,
          selection.end.line,
          selection.end.character,
        );
        if (!this.editor) {
          return {
            words: 0,
            characters: 0,
            lines: 0,
            readingTime: 0,
          };
        }

        const selectedText = this.editor.document.getText(selectedRange);
        const nbOfWords = getNumberOfWords(selectedText);
        return {
          words: nbOfWords,
          characters: getNumberOfCharacters(selectedText, this.getNumberOfCharactersOptions()),
          lines: getNumberOfLines(selectedText, this.getNumberOfLinesOptions()),
          readingTime: nbOfWords / getWordsPerMinutes(),
        };
      })
      .reduce((acc, curr) => {
        return {
          words: acc.words + curr.words,
          characters: acc.characters + curr.characters,
          lines: acc.lines + curr.lines,
          readingTime: acc.readingTime + curr.readingTime,
        };
      });
  }

  private async getRenderedTextStats(): Promise<TextStats> {
    if (
      !this.editor ||
      !isFileExtensionMarkdown(this.editor.document.fileName) ||
      !getRenderedTextStats()
    ) {
      return emptyStats;
    }

    const text = this.editor.document.getText();
    const textWithoutMarkup = await removeMarkdownSyntax(text);
    return this.getTextStats(textWithoutMarkup);
  }

  private formatStat(
    label: string,
    fileStat: number,
    selectedTextStat: number,
    renderedTextStats: number,
  ): string {
    let formatted = `${this.formatValue(fileStat)} ${this.formatLabel(label, fileStat)}`;
    const extraValues = [];
    if (getDisplaySelectionStats() && selectedTextStat > 1) {
      extraValues.push(`${this.formatValue(selectedTextStat)} selected`);
    }
    if (getRenderedTextStats() && renderedTextStats > 1) {
      extraValues.push(`${this.formatValue(renderedTextStats)} rendered`);
    }
    const formattedExtraValues = extraValues.join(", ");
    if (formattedExtraValues) {
      formatted = `${formatted} (${formattedExtraValues})`;
    }

    return formatted;
  }

  private formatLabel(label: string, value: number): string {
    const splitWords = label.replace(/([A-Z])/g, " $1");
    const formattedLabel = splitWords.toLowerCase();

    if (value > 1) {
      return formattedLabel;
    }

    return formattedLabel.replace(/s$/, "");
  }

  private formatValue(value: number): string {
    return new Intl.NumberFormat(vscode.env.language).format(Math.round(value));
  }

  private async show() {
    await this.update();
    this.statusBar.show();
  }

  private hide() {
    this.statusBar.hide();
  }

  public run(editor: vscode.TextEditor) {
    if (isFileExtensionAllowed(editor.document.fileName)) {
      this.editor = editor;
      this.show();
    } else {
      this.editor = null;
      this.hide();
    }
  }
}
