// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";
import { Stats } from "./stats";

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
  const stats = new Stats();

  if (vscode.window.activeTextEditor) {
    stats.run(vscode.window.activeTextEditor);
  }

  const subscriptions = [
    vscode.window.onDidChangeActiveTextEditor(() => {
      if (!vscode.window.activeTextEditor) {
        return;
      }

      stats.run(vscode.window.activeTextEditor);
    }),
    vscode.workspace.onDidChangeConfiguration(() => {
      if (!vscode.window.activeTextEditor) {
        return;
      }

      stats.run(vscode.window.activeTextEditor);
    }),
    vscode.window.onDidChangeTextEditorSelection(() => {
      if (!vscode.window.activeTextEditor) {
        return;
      }

      stats.run(vscode.window.activeTextEditor);
    }),
  ];

  context.subscriptions.push(...subscriptions);
}

// This method is called when your extension is deactivated
export function deactivate() {}
