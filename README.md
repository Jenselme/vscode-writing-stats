# writing-stats

## Features

Display the number of words, lines, characters & read time in the status bar. It displays the stats for the whole document and the selection.

It is inspired by [Marky Stats](https://github.com/robole/vscode-marky-stats/) with extra features.

## Extension Settings

This extension contributes the following settings:

* `writingStats.allowedExtension`: A comma separated list of extension on which to enable the stats.
* `writingStats.countFormattingCharacters`: Count the number of formatting characters like * or ` in markdown.
* `writingStats.countSpaces`: Count the number of spaces.
* `writingStats.countEmptyLines`: Count the number of empty lines.
* `writingStats.displaySelectionStats`: Display the stats in the current selection.
* `writingStats.displayRenderedStats`: Display the stats for the rendered text (ie with all markup removed). This only works on markdown from now.
* `writingStats.displayWordsCount`: Display the number of words.
* `writingStats.displayCharactersCount`: Display the number of characters.
* `writingStats.displayLinesCount`: Display the number of lines.
